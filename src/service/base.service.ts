import axios from 'axios';

import { DataRequest, DataRespone, SortOrder, Dictionary } from '../model/base.model'

declare var process : {
    env: {
        NODE_ENV: string,
        MOCK_DATA: boolean,
        MOCKDATA_ROOT: string
    }
}

export class EnvironmentHelper {    
    public static get isMockData(): boolean {
        return process.env.MOCK_DATA === true;
    }

    public static get mockAppRoot(): string {
        return process.env.MOCKDATA_ROOT;
    }

    public static get isDevelopment(): boolean {
        return process.env.NODE_ENV === "development";
    }

    public static get isProduction(): boolean {
        return process.env.NODE_ENV === "production";
    }
}

export class HttpService {
    constructor(private _path: string) {
    }

    httpGet<X>(endpoint?: string, params?: Dictionary) : Promise<DataRespone<X>> {
        if (EnvironmentHelper.isMockData){
            let qString = "";
            if (params) {
                if (params.sortBy)
                    qString = `_sort=${params.sortBy}&_order=${params.sortOrder == SortOrder.DESC ? "desc" : "asc"}`;

                if (params.pageNumber > 0 && params.pageSize > 0)
                    qString += `&_page=${params.pageNumber}&_limit=${params.pageSize}`;

                if (params.keyword)
                    qString += `&q=${params.keyword}`;

                Object.keys(params).forEach(key => {
                    if (!["sortBy", "sortOrder", "pageNumber", "pageSize", "keyword"].some(k => k == key))
                        qString += `&${key}=${params[key]}`;
                });
            }

            let path = this._path;
            if (endpoint) path = `${path}-${endpoint}`;

            return axios.get(`${EnvironmentHelper.mockAppRoot}/${path}?${qString}`).then(json => {
                let items = json.data;
    
                let response = new DataRespone<X>();
                if (params) {
                    response.pageNumber = params.pageNumber;
                    response.pageSize = params.pageSize;
                    response.sortBy = params.sortBy;
                    response.sortOrder = params.sortOrder;
                    response.keyword = params.keyword;
                }

                response.totalRecords = items.length;
                if (json.headers["x-total-count"])
                    response.totalRecords = parseInt(json.headers["x-total-count"]);
    
                response.items = items;
    
                return response;
            });
        }
        else {
            let path = this._path;
            if (endpoint) path = `${path}/${endpoint}`;

            return axios.get<DataRespone<X>>(`/api/${path}`, { params: params }).then(response => {
                return response.data;
            });
        }                           
    }
}

export class BaseService<T> extends HttpService {
    constructor(protected path: string) {
        super(path);
    }

    getItems(request: DataRequest) : Promise<DataRespone<T>> {
        let params: any = {};
        if (request.keyword)
            params.keyword = request.keyword;
        if (request.sortBy)
            params.sortBy = request.sortBy;
        if (request.sortOrder)
            params.sortOrder = request.sortOrder;
        if (request.pageSize)
            params.pageSize = request.pageSize;
        if (request.pageNumber)
            params.pageNumber = request.pageNumber;

        return this.httpGet<T>(null, params);
    }

}