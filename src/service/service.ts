import axios from 'axios';
import { BaseService } from './base.service';
import { DataRespone } from '../model/base.model';
import { ClientModel, InvoiceModel, ScheduleModel, DashboardStatsModel, ChartModel } from '../model/model';

export class ClientService extends BaseService<ClientModel> {
   constructor() {
       super('client');
   }   
}

export class InvoiceService extends BaseService<InvoiceModel> {
    constructor(){
        super('invoice');
    }
 }

 export class ScheduleService extends BaseService<ScheduleModel> {
    constructor(){
        super('schedule');
    }    
 }

 export class DashboardService extends BaseService<any> {
    constructor() {
        super('dashboard');
    }   

    getStats() {
        return this.httpGet<DashboardStatsModel>('getstats');
    }

    getChart(name: string) {
        return this.httpGet<ChartModel>('getchart', { "name" : name });
    }
 }