export interface Dictionary {
    [index: string]: any
}

export class BaseModel {
    public id: number;
    public title: string;
    public description: string;
}

export enum SortOrder {
    ASC = 1,
    DESC = 2
}

export class DataRequest {
    pageSize: number = 10;
    pageNumber: number = 1;
    sortBy: string;
    sortOrder: SortOrder;
    keyword: string;
}

export class DataRespone<T> {
    items: T[];

    pageNumber: number;
    pageSize: number;
    sortBy: string;
    sortOrder: SortOrder;
    totalRecords: number;
    keyword: string;

    constructor() {
        this.items = [];
        this.pageNumber = 1;
    }    
}