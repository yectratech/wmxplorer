import { BaseModel } from "./base.model";

export class ClientModel extends BaseModel {
    public firstName: string;
    public lastName: string;
    public email: string;
    public revenue: number;
    public aum: number;
}

export class InvoiceModel {
    invoiceNumber: number; 
    clientName: string;
    accountNumber: number;
    custodian: string;
    invoiceDate: Date;
    amount: number;

    get title() {
        return this.invoiceNumber;
    }
}

export class ScheduleModel extends BaseModel {
    public count: number;
}

export class DashboardStatsModel extends BaseModel {
    public amount: number;
    public percent: number;
}

export class ChartModel {
    name: string;
    data: Array<Array<any>>;
}