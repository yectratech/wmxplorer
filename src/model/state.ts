import { DataRespone } from "./base.model";
import { InvoiceModel, ChartModel, ClientModel } from "./model";

export interface InvoiceState {
    invoiceData: DataRespone<InvoiceModel>;    
}

export interface ClientState {
    clientData: DataRespone<ClientModel>;    
}

export interface ChartState {
    chartData: ChartModel; 
    bchartData:ChartModel;   
}