import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/app/home/home.vue'
import ContactUs from '@/app/contact-us/contact-us.vue'
import Invoices from '@/app/invoices/invoices.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/contact-us',
      name: 'ContactUs',
      component: ContactUs
    },
    {
      path: '/invoices',
      name: 'Invoices',
      component: Invoices
    }
  ]
})
