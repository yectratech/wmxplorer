import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { DataRequest, DataRespone } from '../model/base.model';
import { InvoiceModel } from '../model/model';
import { InvoiceState } from '../model/state';
import { InvoiceService } from '../service/service';

export const state: InvoiceState = {
    invoiceData: new DataRespone<InvoiceModel>()
};

export const getters: GetterTree<InvoiceState, any> = {
    invoiceData: state => state.invoiceData
}   

export const mutations: MutationTree<InvoiceState>  = {
    setInvoices(state, data) {
        state.invoiceData = data;
    }
}

export const actions: ActionTree<InvoiceState, any> = {
    getInvoices(context, request: DataRequest) {
        let service = new InvoiceService();

        if (!request)
            request = new DataRequest();

        service.getItems(request)
            .then(response => {
                context.commit('setInvoices', response);
            });
    }
}

export const InvoiceStore = {
    state,
    getters,
    mutations,
    actions
}