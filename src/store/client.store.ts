import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { DataRequest, DataRespone } from '../model/base.model';
import { ClientModel } from '../model/model';
import { ClientState } from '../model/state';
import { ClientService } from '../service/service';

export const state: ClientState = {
    clientData: new DataRespone<ClientModel>()
};

export const getters: GetterTree<ClientState, any> = {
    byRevenue: state => {
        return [...state.clientData.items].sort((c1, c2) => c2.revenue - c1.revenue).slice(0, 5);
    },
    byAssets: state => {
        return [...state.clientData.items].sort((c1, c2) => c2.aum - c1.aum).slice(0, 5);
    }
}   

export const mutations: MutationTree<ClientState>  = {
    setData(state: ClientState, data: DataRespone<ClientModel>) {
        state.clientData = data;
    }
}

export const actions: ActionTree<ClientState, any> = {
    getClients(context) {
        let service = new ClientService();
        let request = new DataRequest();

        service.getItems(request)
            .then(response => {
                context.commit('setData', response);
            });
    }
}

export const ClientStore = {
    state,
    getters,
    mutations,
    actions
}