import Vue from 'vue';
import Vuex from 'vuex';

import { InvoiceStore } from './invoice.store';
import { ChartStore } from './chart.store';
import { ClientStore } from './client.store';

Vue.use(Vuex);


export const store = new Vuex.Store({
    modules: {
        InvoiceStore,
        ChartStore,
        ClientStore
    }
});

