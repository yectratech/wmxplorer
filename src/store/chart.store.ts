import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { ChartModel } from '../model/model';
import { ChartState } from '../model/state';
import { DashboardService } from '../service/service';

export const state: ChartState = {
    chartData: new ChartModel (),
    bchartData: new ChartModel ()
};

export const getters: GetterTree<ChartState, any> = {
    chartData: state => state.chartData,
    bchartData: state => state.bchartData
}   

export const mutations: MutationTree<ChartState>  = {
    setChartData(state, data) {
        state.chartData = data;
    },
    setBChartData(state, data) {
        state.bchartData = data;
    }
}

export const actions: ActionTree<ChartState, any> = {
    getChartData(context, name: string) {
        let service = new DashboardService();
        service.getChart(name).then(response => {
            let chartData: ChartModel;
            if (response.items && response.items.length)
                chartData = response.items[0];

            context.commit('setChartData', chartData);
        });
    },

    getBillStatsChart(context, name: string) {
        let service = new DashboardService();
        service.getChart(name).then(response => {
            let chartData: ChartModel;
            if (response.items && response.items.length)
                chartData = response.items[0];

            context.commit('setBChartData', chartData);
        });
    }
}

export const ChartStore = {
    state,
    getters,
    mutations,
    actions
}