import { SortOrder, DataRespone, DataRequest } from '../../model/base.model';

export class GridModel<T> {
    pageNumber: number = 1;
    pageSize: number = 10;
    totalRecords: number;
    totalPages: number;

    sortOrder: SortOrder;
    sortBy: string;
    status: number;
    keyword: string;
    

    items: Array<GridItemModel<T>> = [];

    constructor(response: DataRespone<T>) {
        this.totalRecords = response.totalRecords;
        this.pageNumber = response.pageNumber;
        this.pageSize = response.pageSize;
        this.sortBy = response.sortBy;
        this.sortOrder = response.sortOrder;
        this.totalRecords = response.totalRecords;
        this.keyword = response.keyword;

        let rowId = 1;
        this.items = response.items.map(i => {
            let item = new GridItemModel(i);
            item.rowId = rowId++;

            return item;
        });
        
        if (this.pageSize == 0)
        {
            if (this.totalRecords > 0)
                this.totalPages = 1;
            else
                this.totalPages = 0;
        }
        else
            this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
    }

    get dataRequest(){
        let request = new DataRequest();

        request.pageSize = this.pageSize;    
        request.pageNumber = this.pageNumber;    
        request.sortBy = this.sortBy;    
        request.sortOrder = this.sortOrder;    
        request.keyword = this.keyword;    

        return request;
    }
}

export class GridItemModel<T> {
    rowId: number;
    selected: boolean;

    constructor(public data: T){}
}

export class GridColumn {
    fieldName: string;
    label: string;
    dataType: string;

    sortable: boolean = true;
    sortBy: string;

    filterable: boolean;

    hidden: boolean;

    formatter: Function = v => v;

    template: any;
}

export class GridRow {
    item: GridItemModel<any>;
    columns: Array<GridColumn>;

    constructor(item: GridItemModel<any>, columns: Array<GridColumn>) {
        this.item = item;
        this.columns = columns;
    }

    getValue(columnName) {
        return this.item.data[columnName];
    }
}