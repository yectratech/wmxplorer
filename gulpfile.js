var gulp = require('gulp');
var sass = require("gulp-sass");
var compass = require("compass-importer");
var print = require('gulp-print').default;

gulp.task('sass', function () {
    return gulp.src('./css/sass/**/*.scss')
        .pipe(sass({ importer: compass, outputStyle: 'compressed' }))
        .pipe(gulp.dest('./css'))
        .pipe(print(function () {
            return 'css file created successfully!';
        }));
});

gulp.task('watch-sass', function () {
    gulp.watch('./css/sass/**/*.scss', gulp.series('sass'));
});
