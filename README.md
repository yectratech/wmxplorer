# Vue Vuex TypeScript Prototype

Prototype developed to demonstrate Vue Vuex Typescript competence

## Steps to setup and run the prototype

- Install [visual studio code](https://code.visualstudio.com/) Free source-code editor with support for Git control, syntax highlighting, intelligent code completion, snippets, code refactoring etc...

- Install [node.js](https://nodejs.org/en/) 

- If you don't have Git installed in your computer install Git (https://git-scm.com/) and a Git Client (https://git-scm.com/downloads/guis). 

- Download the repository and open the folder with visual studio code

- Open a new terminal (Terminal > New Terminal) and execute the command "npm install". This will install all necessary packages. 

- Open another terminal and execute the command "npm install -g json-server". This is for mocking the API calls and providing dynamic access to data 

- Open another terminal and execute the command "json-server db.json". This will launch the API service at http://localhost:3000

- Open another terminal and execute "npm start dev". This should launch your browser and the prototype will be up and running.

## Steps to compile sass file to css

- Open a new terminal (Terminal > New Terminal) and execute the command "npm install -g gulp". [gulp](https://gulpjs.com/) For compiling sass files

- Open another terminal and execute the command "gulp watch-sass". Now any changes to sass file will regenerate css file

